var res = {
    Menu_Background_png: "res/menu/menu_background.png",
    Menu_Begin_btn_png: "res/menu/begin_btn.png",
    Menu_Begin_btn_hover_png: "res/menu/begin_btn_down.png",
    
    Game_Background_png: "res/game/background.png",
    Game_Gem_png: "res/game/gem.png",
    Game_End_btn_png: "res/game/end_game_btn.png",
    Game_End_btn_hover_png: "res/game/end_game_btn_hover.png",

    PixelNoirFont: { fontName:"Pixel-Noir-Regular",
        src:[
            {src:"res/Pixel-Noir-Regular.eot", type:"embedded-opentype"},
            {src:"res/Pixel-Noir-Regular.ttf", type:"truetype"}]}
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}