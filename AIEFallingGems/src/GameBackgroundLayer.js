var GameBackgroundLayer = cc.Layer.extend({
	backgroundSprite: null,
	
	ctor: function() {
		this._super();
	},
	
	init: function() {
		var size = cc.winSize;
		
		this.backgroundSprite = new cc.Sprite(res.Game_Background_png);
		this.backgroundSprite.attr({
			x: size.width / 2,
			y: size.height / 2,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		this.addChild(this.backgroundSprite);
	},
});