var SCORE_TEXT_LABEL_FONT_SIZE 	= 24;
var SCORE_FONT_SIZE 			= 48;
var WEATHER_FONT_SIZE 			= 14;
var WEATHER_UPDATE_TIME 		= 20000;

var WEATHER_SERVICE_URL  = "http://api.openweathermap.org/data/2.5/weather";
var WEATHER_SERVICE_ARGS = "?q=Melbourne,%20Australia&units=metric";

var GameStatusLayer = cc.Layer.extend({
	scoreTextLabel: null,
	scoreLabel: null,

	weatherLabel: null,
	
	ctor: function() {
		this._super();
		
	},
	
	init: function() {
		var size = cc.winSize;
		
		this.scoreTextLabel = new cc.LabelTTF("score", g_FontName, SCORE_TEXT_LABEL_FONT_SIZE);
		this.scoreTextLabel.attr({
			x: 20,
			y: 160,
			anchorX: 0,
			anchorY: 0
		});
		
		this.scoreLabel = new cc.LabelTTF("0", g_FontName, SCORE_FONT_SIZE);
		this.scoreLabel.attr({
			x: 20,
			y: 110,
			anchorX: 0,
			anchorY: 0
		});

		this.weatherLabel = new cc.LabelTTF("", g_FontName, WEATHER_FONT_SIZE);
		this.weatherLabel.attr({
			x: 5,
			y: 5,
			anchorX: 0,
			anchorY: 0
		});
		
		this.addChild(this.scoreTextLabel);
		this.addChild(this.scoreLabel);
		this.addChild(this.weatherLabel);
		
		this.createUI();
		this.updateWeather();
	},
	
	createUI: function() {
		var size = cc.winSize;
		
		var endItem = new cc.MenuItemImage(
				res.Game_End_btn_png,
				res.Game_End_btn_hover_png,
				function() {
					cc.log("end game clicked");
					
					var gameLayer = this.getParent().getChildByTag(LayerTags.GameLayer);
					gameLayer.gameOver(GameOverReasons.EndedGame);
				},
				this);

		endItem.attr({
			x: size.width - 5,
			y: 5,
			anchorX: 1.0,
			anchorY: 0.0
		});

		var menu = new cc.Menu(endItem);
		menu.x = 0;
		menu.y = 0;

		this.addChild(menu);
	},

	updateWeather: function() {
		var statusLayer = this;

		cc.loader.loadJson(WEATHER_SERVICE_URL + WEATHER_SERVICE_ARGS, function(error, data) {
			if(data) {
				var weatherData = data.name + ": " + data.main.temp 
					+ "C (min " + data.main.temp_min 
					+ ", max " + data.main.temp_max + ")";

				statusLayer.weatherLabel.setString(weatherData);
			}

			setTimeout(function() { statusLayer.updateWeather(); }, WEATHER_UPDATE_TIME);
		});
	},
	
	setScore: function(amt) {
		this.scoreLabel.setString(amt);
	},
});