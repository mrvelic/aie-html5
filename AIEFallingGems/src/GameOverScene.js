var GameOverLayer = cc.Layer.extend({
	reason: null,
	
	ctor: function(reason) {
		this._super();
		this.reason = reason;
		
		this.init();
	},

	init: function() {
		var size = cc.winSize;

		var background = new cc.Sprite(res.Menu_Background_png);
		background.attr({
			x: size.width / 2,
			y: size.height / 2,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		// reason for game over
		var gameOverText = new cc.LabelTTF(this.reason, g_FontName, 32);
		gameOverText.attr({
			x: size.width / 2,
			y: size.height - 220,
			anchorX: 0.5,
			anchorY: 0.5,
			color: new cc.Color(255, 255, 255, 255)
		});
		
		// reason for game over
		var highScoresText = new cc.LabelTTF("High Scores", g_FontName, 28);
		highScoresText.attr({
			x: size.width / 2,
			y: size.height - 260,
			anchorX: 0.5,
			anchorY: 0.5,
			color: new cc.Color(255, 200, 200, 255)
		});
	
		// high scores
		var scores = HighScores.getScores();
		var scoreLabels = [];
		var scoreStartY = 300;
		var scoreRemoveY = 48;
		var maxScores = 5;
		var numScores = 0;
		
		for(var i in scores) {
			if(numScores >= maxScores) break;
			
			// score label
			var scoreLabel = new cc.LabelTTF(scores[i].toString(), g_FontName, 48);
			scoreLabel.attr({
				x: size.width / 2,
				y: size.height - scoreStartY,
				anchorX: 0.5,
				anchorY: 0.5,
				color: new cc.Color(255, 255, 255, 255)
			});
			
			scoreLabels.push(scoreLabel);
			
			scoreStartY += scoreRemoveY;
			numScores++;
		}

		var beginItem = new cc.MenuItemImage(
				res.Menu_Begin_btn_png,
				res.Menu_Begin_btn_hover_png,
				function() {
					cc.log("begin game clicked");
					cc.director.runScene(new GameScene());
				},
				this);

		beginItem.attr({
			x: size.width / 2,
			y: size.height / 4,
			anchorX: 0.5,
			anchorY: 0.5
		});

		var menu = new cc.Menu(beginItem);
		menu.x = 0;
		menu.y = 0;

		this.addChild(background);
		this.addChild(menu);
		this.addChild(gameOverText);
		this.addChild(highScoresText);
		
		for(var i in scoreLabels) {
			this.addChild(scoreLabels[i]);
		}
	}
});

var GameOverScene = cc.Scene.extend({
	reason: "Game Over",
	
	ctor: function(reason) {
		this._super();
		
		this.reason = reason;
	},
	
	onEnter: function() {
		this._super();

		this.addChild(new GameOverLayer(this.reason));
	}
});