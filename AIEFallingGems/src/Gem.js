var GEM_ORIGINAL_WIDTH = 32.0;

var Gem = cc.Class.extend({
	type: null,
	sprite: null,
	position: null,
	gameLayer: null,
	gridX: 0,
	gridY: 0,
	onOpenList: false,
	
	ctor: function(gameLayer, type, position, width) {
		this.type = type;
		this.position = position;
		this.gameLayer = gameLayer;
		
		this.initSprite();
		this.setType(type);
		this.setWidth(width);
	},
	
	initSprite: function() {
		this.sprite = new cc.Sprite(res.Game_Gem_png);
		
		this.sprite.attr({
			x: this.position.x,
			y: this.position.y,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		this.gameLayer.addChild(this.sprite);
	},
	
	setPosition: function(position) {
		this.position = position;
		
		this.sprite.attr({
			x: position.x,
			y: position.y,
		});
		
		/*
		this.sprite.runAction(
			cc.moveTo(1.0, position)
		);*/
	},
	
	setType: function(type) {
		this.type = type;
		
		var color = GemColors[this.type];
		this.sprite.setColor(color);
	},
	
	setWidth: function(width) {
		this.sprite.setScale(width / GEM_ORIGINAL_WIDTH);
	},
	
	setGridRef: function(x, y) {
		this.gridX = x;
		this.gridY = y;
	},
	
	getPosition: function() { return this.position; },
	getType: function() { return this.type; },
	getGridX: function() { return this.gridX; },
	getGridY: function() { return this.gridY; },
});