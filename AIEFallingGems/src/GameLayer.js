var GameLayer = cc.Layer.extend({
	grid: [],
	fallingGems: [],
	currentMatchSeq: [],
	gemsToRemove: [],
	
	gameSettings: null,
	currentScore: 0,
	numConsecMatches: 0,
	
	gridOffsetX: 72,
	gridOffsetY: 50,
	gridMinY: 200,
	
	gridMaxWidth: 0,
	gridMaxHeight: 0,
	
	gridWidth: 0,
	gridHeight: 0,
	
	gemSize: 0,
	
	gemFallTimer: 0.0,
	
	ctor: function() {
		this._super();
		this.createEventHandlers();
	},
	
	init: function() {
		var instance = this;
		
		cc.loader.loadJson(Levels[0], function(error, data) {
			cc.log("Loaded level...");
			cc.log(data);
			
			instance.gameSettings = data;
			
			instance.initGame();
			instance.scheduleUpdate();
		});
	},
	
	initGame: function() {
		this.cleanGridMemory();
		
		this.fallingGems = [];
		this.currentScore = 0;
		this.currentMultiplier = 1.0;
		this.gemFallTimer = 0.0;
		
		this.calcGridMaxes();
		this.initGrid();
		
		this.createFallingGemsSet();
	},
	
	createEventHandlers: function() {
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyReleased: function(key, event) {
				event.getCurrentTarget().onKeyUp(key, event);
			}
		}, this);
		
		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ALL_AT_ONCE,
			onTouchesEnded: function(touches, event) {
				event.getCurrentTarget().onTouchesEnded(touches, event);
			}
		}, this);
	},
	
	cleanGridMemory: function() {
		if(this.grid.length > 0) {
			for(var y = 0; y < this.grid.length; y++) {
				for(var x = 0; x < this.grid[y].length; x++) {
					if(this.grid[y][x] !== GemTypes.Empty) {
						this.removeChild(this.grid[y][x]);
					}
				}
			}
		}
	},
	
	calcGridMaxes: function() {
		var size = cc.winSize;
		
		this.gridMaxWidth 	= size.width  - (this.gridOffsetX * 2);
		this.gridMaxHeight 	= size.height - (this.gridOffsetY + this.gridMinY);
		
		var gemSizeX = this.gridMaxWidth  / this.gameSettings.cols;
		var gemSizeY = this.gridMaxHeight / this.gameSettings.rows;
		
		this.gemSize = Math.min(gemSizeX, gemSizeY);
		
		this.gridWidth  = this.gemSize * this.gameSettings.cols;
		this.gridHeight = this.gemSize * this.gameSettings.rows;
	},
	
	getPositionFromGridRef: function(x, y) {
		var posX = (this.gridOffsetX + (x * this.gemSize)) + (this.gemSize / 2);
		var posY = (this.gridHeight  - (y * this.gemSize)) + (this.gemSize / 2) + (this.gridOffsetY + this.gridMinY);
		
		return new cc.p(posX, posY);
	},
	
	initGrid: function() {
		this.grid = [];
		
		// create empty grid
		for(var y = 0; y < this.gameSettings.rows; y++) {
			this.grid[y] = [];
		}
		
		for(var x = 0; x < this.gameSettings.cols; x++) {
			for(var y = 0; y < this.gameSettings.rows; y++) {
				this.grid[y][x] = GemTypes.Empty;
			}
		}
		
		// fill up to max gems
		var gemsCreated = 0;
		
		for(var y = (this.gameSettings.rows -1); y >= 0; y--) {
			for(var x = 0; x < this.gameSettings.cols; x++) {
				if(gemsCreated == this.gameSettings.startingGems) break;
				
				this.addRandomGemAtPosition(x, y);
				
				gemsCreated++;
			}
		}
	},
	
	createFallingGemsSet: function() {
		this.fallingGems = [];
		
		// check it even fits on the grid
		if(this.gameSettings.numFallingGems > this.gameSettings.rows) {
			this.gameOver(GameOverReasons.HitCeiling);
			return;
		}
		
		var availableCols = [];
		// check we will fit anywhere on the grid
		for(var x = 0; x < this.gameSettings.cols; x++) {
			var spaceInCol = true;
			for(var y = 0; y < this.gameSettings.numFallingGems; y++) {
				// if not empty, theres not enough space in this column
				if(this.grid[y][x] !== GemTypes.Empty) {
					spaceInCol = false;
				}
			}
			
			if(spaceInCol) {
				availableCols.push(x);
			}
		}
		
		// if no available cols, game over
		if(availableCols.length == 0) {
			this.gameOver(GameOverReasons.HitCeiling);
			return;
		}
		
		// pick a random column
		var colToSpawn = availableCols[Math.floor((Math.random() * availableCols.length))];
		
		// spawn gems in column
		for(var y = 0; y < this.gameSettings.numFallingGems; y++) {
			var gem = this.addRandomGemAtPosition(colToSpawn, y);
			this.fallingGems[y] = gem;
		}
	},
	
	addRandomGemAtPosition: function(x, y) {
		var type = Math.floor((Math.random() * GemTypeRange[1]) + GemTypeRange[0]);
		var gem = new Gem(this, type, this.getPositionFromGridRef(x, y), this.gemSize);
		
		this.grid[y][x] = gem;
		
		gem.setGridRef(x, y);
		return gem;
	},
	
	canMoveFallingGems: function(gems, direction) {
		if(gems.length > 0) {
			var lastGem = gems[gems.length - 1];
			
			if(lastGem) {
				var gemX = lastGem.getGridX();
				var gemY = lastGem.getGridY();
				
				gemX += direction.x;
				gemY += direction.y;
				
				if(gemY < 0) {
					return false;
				}
				
				if(gemY >= this.gameSettings.rows) {
					return false;
				}
				
				if(gemX < 0) {
					return false;
				}
				
				if(gemX >= this.gameSettings.cols) {
					return false;
				}
				
				if(this.grid[gemY][gemX] === GemTypes.Empty) {
					return true;
				}
			}
		}
		
		return false;
	},
	
	moveFallingGems: function(is_player, gems, direction) {
		var shouldFindMatches = false;
		var resetFallingGems = false;
		
		if(this.canMoveFallingGems(gems, direction)) {
			for(var i = (gems.length - 1); i >= 0 ; i--) {
				var gem = gems[i];
				
				if(gem) {
					var gemX = gem.getGridX();
					var gemY = gem.getGridY();
					
					gem = this.grid[gemY][gemX];
					if(gem !== GemTypes.Empty) {
						this.grid[gemY][gemX] = GemTypes.Empty;
						
						// move in direction
						gemX += direction.x;
						gemY += direction.y;
						
						gem.setGridRef(gemX, gemY);
						this.grid[gemY][gemX] = gem;
						
						gem.setPosition(this.getPositionFromGridRef(gemX, gemY));
					}
				}
			}
			
			if(gems.length > 0) {
				var last = gems[gems.length - 1];
				var gemX = last.gridX;
				var gemY = last.gridY;
				gemY++;

				if(gemY < this.gameSettings.rows) {
					if(this.grid[gemY][gemX] !== GemTypes.Empty) {
						shouldFindMatches = true;

						if(is_player) {
							resetFallingGems = true;
						}
					}
				}

				if(gemY >= this.gameSettings.rows) {
					shouldFindMatches = true;

					if(is_player) {
						resetFallingGems = true;
					}
				}
			}
		} else {
			shouldFindMatches = false;
		}
		
		if(shouldFindMatches) {
			for(var i in gems) {
				var g = this.grid[gems[i].gridY][gems[i].gridX];
				
				this.findMatches(g);
			}
			
			if(resetFallingGems) {
				this.fallingGems = [];
			}
		}
	},
	
	shiftFallingGemsDown: function() {
		this.moveFallingGems(true, this.fallingGems, { x: 0, y: 1 });
	},
	
	shiftFallingGemsLeft: function() {
		this.moveFallingGems(true, this.fallingGems, { x: -1, y: 0 });
	},
	
	shiftFallingGemsRight: function() {
		this.moveFallingGems(true, this.fallingGems, { x: 1, y: 0 });
	},
	
	findMatches: function(gem) {
		cc.log("time to find matches");
		if(gem) {
			var open = [gem,];
			
			while(open.length > 0) {
				var currGem = open[0];
				
				var neighbors = this.findGemNeighbors(currGem);
				
				for(var i in neighbors) {
					var n = neighbors[i];
					
					if(!n.onOpenList) {
						open.push(n);
						n.onOpenList = true;
					}
				}
				
				this.currentMatchSeq.push(currGem);
				open.splice(0, 1);
			}
			
			if(this.currentMatchSeq.length >= this.gameSettings.numRequiredForMatch) {
				for(var i in this.currentMatchSeq) {
					var g = this.currentMatchSeq[i];
					
					g.setType(GemTypes.Active);
					g.onOpenList = false;
					
					this.gemsToRemove.push(g);
					
					this.addScore((this.currentMatchSeq.length * this.gameSettings.scoreMultiplier) * this.gameSettings.scorePerGem);
				}
			}
			
			for(var i in this.currentMatchSeq) {
				this.currentMatchSeq[i].onOpenList = false;
			}
			this.currentMatchSeq = [];
		}
	},
	
	findGemNeighbors: function(gem) {
		var gemX = gem.getGridX();
		var gemY = gem.getGridY();
		
		var neighbors = [];
		var neighborPositions = [
		    new cc.p(gemX + 1,	gemY),
		    new cc.p(gemX - 1, 	gemY),
		    new cc.p(gemX, 		gemY + 1),
		    new cc.p(gemX, 		gemY - 1),
		];
		
		for(var i in neighborPositions) {
			var p = neighborPositions[i];
			
			if(		(p.x >= 0 && p.x < this.gameSettings.cols)
				&&  (p.y >= 0 && p.y < this.gameSettings.rows)) 
			{
				var neighborGem = this.grid[p.y][p.x];
				if(neighborGem !== GemTypes.Empty) {
					if(neighborGem.getType() === GemTypes.Active) continue;
					
					if(gem.getType() === neighborGem.getType()) {
						neighbors.push(neighborGem);
					}
				}
			}
		}
		
		return neighbors;
	},
	
	getScoreMultiplier: function() {
		return this.numConsecMatches * this.gameSettings.scoreMultiplier;
	},
	
	addScore: function(amt) {
		var scoreLayer = this.getParent().getChildByTag(LayerTags.StatusLayer);
		
		this.currentScore += amt;
		scoreLayer.setScore(this.currentScore);
	},
	
	gameOver: function(reason) {
		cc.log("game over :( " + reason);
		
		HighScores.addScore(this.currentScore);
		cc.director.runScene(new GameOverScene(reason));
	},
	
	update: function(dt) {
		if(this.fallingGems.length > 0) {
			this.gemFallTimer += dt;
			
			if(this.gemFallTimer > this.gameSettings.fallSpeed) {
				this.shiftFallingGemsDown();
				this.gemFallTimer = 0.0;
			}
		} else {
			this.gemFallTimer = 0.0;
		}
		
		for(var i in this.gemsToRemove) {
			this.removeGem(this.gemsToRemove[i]);
		}
		this.gemsToRemove = [];
		
		this.shiftAllGemsDown();
		
		// check gems are in correct positions
		this.checkGemPositions();
		
		if(this.fallingGems.length == 0) {
			this.createFallingGemsSet();
		}

		if(this.checkWinCondition()) {
			this.gameOver(GameOverReasons.AllGemsGone);
		}
	},
	
	shiftAllGemsDown: function() {
		for(var y = (this.grid.length - 1); y >= 0; y--) {
			for(var x in this.grid[y]) {
				var g = this.grid[y][x];
				if(g !== GemTypes.Empty) {
					var matchesFallingGem = false;
					
					for(var i in this.fallingGems) {
						var fg = this.fallingGems[i];
						
						if((g.gridX == fg.gridX) && (g.gridY == fg.gridY)) {
							matchesFallingGem = true;
						}
					}
					
					if(!matchesFallingGem) {
						this.moveFallingGems(false, [g,], { x: 0, y: 1 });
					}
				}
			}
		}
	},
	
	removeGem: function(gem) {
		if(gem !== GemTypes.Empty) {
			var gemX = gem.getGridX();
			var gemY = gem.getGridY();
			
			var sprite = this.grid[gemY][gemX].sprite;
			
			if(sprite) {
				sprite.runAction(
					new cc.FadeOut(0.4)
				);
			}
			
			this.grid[gemY][gemX] = GemTypes.Empty;
		}
	},
	
	checkGemPositions: function() {
		for(var y = 0; y < this.grid.length; y++) {
			for(var x = 0; x < this.grid[y].length; x++) {
				var gem = this.grid[y][x];
				
				if(gem !== GemTypes.Empty) {
					var currPos = gem.getPosition();
					var correctPos = this.getPositionFromGridRef(gem.getGridX(), gem.getGridY());
					
					if(currPos.x !== correctPos.x || currPos.y !== correctPos.y) {
						gem.setPosition(correctPos);
					}
				}
			}
		}
	},

	checkWinCondition: function() {
		var gameWon = true;

		for(var y = 0; y < this.grid.length; y++) {
			for(var x in this.grid[y]) {
				var g = this.grid[y][x];
				if(g !== GemTypes.Empty) {
					var matchesFallingGem = false;
					
					for(var i in this.fallingGems) {
						var fg = this.fallingGems[i];
						
						if((g.gridX == fg.gridX) && (g.gridY == fg.gridY)) {
							matchesFallingGem = true;
						}
					}
					
					if(!matchesFallingGem) {
						gameWon = false;
					}
				}
			}
		}

		return gameWon;
	},
	
	// movement handling
	onKeyUp: function(key, event) {
		if(key === cc.KEY.left) {
			this.shiftFallingGemsLeft();
		}
		
		if(key === cc.KEY.right) {
			this.shiftFallingGemsRight();
		}
		
		if(key == cc.KEY.down) {
			this.shiftFallingGemsDown();
		}
	},
	
	onTouchesEnded: function(touches, event) {
		var touch = touches[0];
		
		var size = cc.winSize;
		var pos = touch.getLocation();
		
		if(pos.x > (size.width / 2) && pos.y > 200.0) {
			this.shiftFallingGemsRight();
		}
		
		if(pos.x < (size.width / 2) && pos.y > 200.0) {
			this.shiftFallingGemsLeft();
		}
		
		if(pos.y < 200.0) {
			this.shiftFallingGemsDown();
		}
	},
});