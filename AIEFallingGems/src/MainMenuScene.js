var MainMenuLayer = cc.Layer.extend({
	ctor: function() {
		this._super();
		this.init();
	},
	
	init: function() {
		var size = cc.winSize;
		
		var background = new cc.Sprite(res.Menu_Background_png);
		background.attr({
			x: size.width / 2,
			y: size.height / 2,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		var helpText = new cc.LabelTTF("Touch to the left and right of the screen to move.\nOr use the arrow keys.", g_FontName, 18);
		helpText.attr({
			x: size.width / 2,
			y: (size.height / 2) - 100,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		var beginItem = new cc.MenuItemImage(
			res.Menu_Begin_btn_png,
			res.Menu_Begin_btn_hover_png,
			function() {
				cc.log("begin game clicked");
				cc.director.runScene(new GameScene());
			},
			this);
		
		beginItem.attr({
			x: size.width / 2,
			y: size.height / 2,
			anchorX: 0.5,
			anchorY: 0.5
		});
		
		var menu = new cc.Menu(beginItem);
		menu.x = 0;
		menu.y = 0;
		
		this.addChild(background);
		this.addChild(menu);
		this.addChild(helpText);
	}
});

var MainMenuScene = cc.Scene.extend({
	onEnter: function() {
		this._super();
		
		this.addChild(new MainMenuLayer());
	}
});