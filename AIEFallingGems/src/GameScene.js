var GameScene = cc.Scene.extend({
	onEnter: function() {
		this._super();
		
		var background = new GameBackgroundLayer();
		var game = new GameLayer();
		var status = new GameStatusLayer();
		
		background.init();
		game.init();
		status.init();
		
		this.addChild(background, 0, LayerTags.BackgroundLayer);
		this.addChild(game, 0, LayerTags.GameLayer);
		this.addChild(status, 0, LayerTags.StatusLayer);
	},
});