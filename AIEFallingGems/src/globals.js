var g_FontName = "Courier New";

var Levels = [
	'levels/default.json',
];

var LayerTags = {
	MainMenuLayer: 		0,
	BackgroundLayer: 	1,
	GameLayer: 			2,
	StatusLayer: 		3,
};

var GemTypes = {
	Empty: 	0,
	Red: 	1,
	Green: 	2,
	Blue: 	3,
	Yellow: 4,
	Active: 5
};

var GemTypeRange = [GemTypes.Red, GemTypes.Yellow];

var GemColors = [];
GemColors[GemTypes.Empty] 	= new cc.Color(0, 	0, 	 0,   0  );
GemColors[GemTypes.Red] 	= new cc.Color(255, 0, 	 0,   255);
GemColors[GemTypes.Green] 	= new cc.Color(0, 	255, 0,   255);
GemColors[GemTypes.Blue] 	= new cc.Color(0,	0, 	 255, 255);
GemColors[GemTypes.Yellow]	= new cc.Color(255, 255, 0,	  255);
GemColors[GemTypes.Active]	= new cc.Color(255, 255, 255, 255);

var GameOverReasons = {
	HitCeiling: "You burst through the ceiling!",
	AllGemsGone: "You eliminated all the gems!",
	EndedGame: "Game Over",
};

var Tools = {
	inArray: function(haystack, needle) {
		for(var i = 0; i < haystack.length; i++) {
			if(haystack[i] == needle) return true;
		}
		
		return false;
	},
};

var HighScores = {
		scoresKey: "AIE_GEMFALL_HIGHSCORES",
		
		getData: function() {
			var ls = cc.sys.localStorage;

			var data = ls.getItem(this.scoresKey);

			if(data == null) { 
				data = { scores: [0, ] };
				ls.setItem(this.scoresKey, JSON.stringify(data));
			} else {
				data = JSON.parse(data);
			}
			
			return data;
		},
		
		setData: function(data) {
			var ls = cc.sys.localStorage;
			ls.setItem(this.scoresKey, JSON.stringify(data));
		},
		
		getScores: function() {
			var data = this.getData();
			
			return data.scores.sort(function(a, b) { return a < b ? 1 : 0; });
		},
		
		addScore: function(score) {
			cc.log("adding score " + score);

			var data = this.getData();
			data.scores.push(score);

			cc.log(data.scores);
			
			this.setData(data);
		},
};